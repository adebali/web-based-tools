var r = radius;
var x170 = 170


function highlight2(d){
		console.log(d.name)
		d3.select(this).attr("fill",function(d){console.log(d.name); return "red"})
}
function highlight(d){
	if (d.children){
		d.children.forEach(function(n){
			highlight(n);
			});
		}
	else{
		console.log(d.name);
		}
}

function printEventualChildren(d){
	if (d.children){
		d.children.forEach(function(n){
			printEventualChildren(n);
			});
		}
	else{
		console.log(d.name);
	   }
}

var cluster = d3.layout.cluster()
    .size([360, 1])
    .sort(null)
    .value(function(d) { return d.length; })
    .children(function(d) { return d.branchset; })
    .separation(function(a, b) { return 1; });

function project(d) {
  var r = d.y, a = (d.x - 90) / 180 * Math.PI;
  return [r * Math.cos(a), r * Math.sin(a)];
}

function cross(a, b) { return a[0] * b[1] - a[1] * b[0]; }
function dot(a, b) { return a[0] * b[0] + a[1] * b[1]; }

function step(d) {
  var s = project(d.source),
      m = project({x: d.target.x, y: d.source.y}),
      t = project(d.target),
      r = d.source.y,
      sweep = d.target.x > d.source.x ? 1 : 0;
  return (
    "M" + s[0] + "," + s[1] +
    "A" + r + "," + r + " 0 0," + sweep + " " + m[0] + "," + m[1] +
    "L" + t[0] + "," + t[1]);
}

var rwhConstant = 3;

var wrap = d3.select("#vis").append("svg")
    .attr("width", r * rwhConstant)
    .attr("height", r * rwhConstant)
    //.attr("padding-left", "-100px")
    .style("-webkit-backface-visibility", "hidden");

// Catch mouse events in Safari.
wrap.append("rect")
    .attr("width", r * rwhConstant)
    .attr("height", r * rwhConstant)
    .attr("fill", "none")

var vis = wrap.append("g")
    .attr("transform", "translate(" + r*rwhConstant/2 + "," + r*rwhConstant/2+ ")");

var start = null,
    rotate = 0,
    div = document.getElementById("vis");

function mouse(e) {
  return [
    e.pageX - div.offsetLeft - r,
    e.pageY - div.offsetTop - r
  ];
}
/*
wrap.on("mousedown", function() {
  wrap.style("cursor", "move");
  start = mouse(d3.event);
  d3.event.preventDefault();
});
d3.select(window)
  .on("mouseup", function() {
    if (start) {
      wrap.style("cursor", "auto");
      var m = mouse(d3.event);
      var delta = Math.atan2(cross(start, m), dot(start, m)) * 180 / Math.PI;
      rotate += delta;
      if (rotate > 360) rotate %= 360;
      else if (rotate < 0) rotate = (360 + rotate) % 360;
      start = null;
      wrap.style("-webkit-transform", null);
      vis
          .attr("transform", "translate(" + r*rwhConstant/2 + "," + r*rwhConstant/2 + ")rotate(" + rotate + ")")
        .selectAll("text")
          .attr("text-anchor", function(d) { return (d.x + rotate) % 360 < 180 ? "start" : "end"; })
          .attr("transform", function(d) {
            return "rotate(" + (d.x - 90) + ")translate(" + (r - x170 + 8) + ")rotate(" + ((d.x + rotate) % 360 < 180 ? 0 : 180) + ")";
          });
    }
  })
  .on("mousemove", function() {
    if (start) {
      var m = mouse(d3.event);
      var delta = Math.atan2(cross(start, m), dot(start, m)) * 180 / Math.PI;
      wrap.style("-webkit-transform", "rotateZ(" + delta + "deg)");
    }
  });
*/

var length2pxConstant = 115;

function phylo(n, offset) {
  //if (n.length != null) offset += n.length * 115;
  if (n.length != null) offset += n.length * length2pxConstant;
  n.y = offset;
  if (n.children)
    n.children.forEach(function(n) {
      phylo(n, offset);
    });
}


function nodeDistance(node){
	if (node.parent){
	var distance = node.length;
	theParent = node.parent;
	while(theParent.parent){
//		console.log("HEEYYYY");
		if(theParent.length){
		distance += theParent.length;
			}
		theParent = theParent.parent;
		}
	return distance;
	}
	else{
//		console.log("No parent")
		return 0;
		}
	}

d3.text(nwkFile, function(text) {
	//d3.csv(csv, function(error,markerData){
	d3.csv("sterol-sensing_fullList.csv", function(error,markerData){
	
	


  var x = newick.parse(text);
  var nodes = cluster.nodes(x);
  phylo(nodes[0], 0);

//console.log(markerData);

var count = 0
nodes.filter(function(n) { return n.children == null; }).forEach(function(d){
	d.m1 = 0;
	markerData.forEach(function(m){
		//console.log(m);
		if (d.name.indexOf(m.gi) > -1){
			//console.log(m.gi + "YES");
			d.m1 = m["sterol-sensing"];
			count++;
			}
	//	else{if (d.name.indexOf('elegans') >-1){console.log(d);}}

	});
});

console.log(count);
//console.log(nodes);


  var link = vis.selectAll("path.link")
      .data(cluster.links(nodes))
    .enter().append("path")
      .attr("class", "link")
      .attr("d", step);

  var node = vis.selectAll("g.node")
      .data(nodes.filter(function(n) { return n.x !== undefined; }))
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })


   node.append("svg:a")
	//.attr("xlink:href",function(d){ return d.children ? "https://www.google.com/#q=" + d.name:"https://www.google.com/#q=" + d.name.replace(/_/g,' ').split('|')[1];})
	.attr("xlink:href","javascript:;")
//	.attr("target","_blank")
        .append("svg:circle")
        .attr('stroke',  function(d){return d.children ? 'black':d.m1?'orange':'yellowGreen';})
        .attr('fill', function(d){return d.children ? 'gray':'greenYellow';})
        .attr('stroke-width', function(d){return d.children ? "2px":"3px";})
      	.attr("r",function(d){return d.children ? 1.5:2.5;})
	.on('click',function(d){printEventualChildren(d);});
//	.on('mouseover', function(d) {
//	if (d.children){
//		highlight(d);	
		//console.log("ASDASD")  
		//d3.select(this).attr("fill",function(d){console.log(d.name); return "red"})
//		}
//	});




  node.append("svg:title")
      .text(function(d){return d.name;});
//      .text(function(d){return d.dx;});
//      .text(function(d){return nodeDistance(d);});
//      .text(function(d){return nodeDistance(d);});




 var mycircle =  node.append("svg:circle")
	//.attr("dy","2px")
	.attr("cx", function(d){return r*3.24/4-(length2pxConstant)*nodeDistance(d);})
//	.attr("dx", function(d){return d.dx + 5;})
	//.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (r - x170 + 8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; })
        .attr('stroke',  function(d){return d.children ? 'white':(d.m1?"red":"white");})
//        .attr('fill', function(d){return d.children ? 'gray':'white';})
        .attr('stroke-width', function(d){return d.children ? "0px":(d.m1?"3px":"0px");})
      	.attr("r",function(d){return d.m1 ? 1.5:0;});
//      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; });


  var label = vis.selectAll("text")
      .data(nodes.filter(function(d) { return d.x !== undefined && !d.children; }))
    .enter()
      .append("a")
      .attr("xlink:href",function(d){ return "http://www.ncbi.nlm.nih.gov/protein/" + d.name.split('gi|')[1].split('|')[0] + "?report=fasta";})
      .attr("target","_blank")
      .append("text")
      .attr("y", ".31em")
      //.attr("fill", "yellowGreen")
      .attr("fill", "black")
      .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (r - x170 + 8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; })
      //.text(function(d) { return d.name.replace(/_/g, ' '); });
      .text(function(d) { return d.name.split('|')[1].replace(/_/g, ' '); })
      //.append("svg:title").text(function(d){return d.name;});
      .append("svg:title").text(function(d){return d.name + "- " + d.m1;});


  var marker = vis.selectAll("svg")
	.data(nodes.filter(function(d) { return d.x !== undefined && !d.children; }))
	.enter().append("rect")
	.attr("y", ".31em")

//	.attr("x", ".31em")
	.attr("height","5px")
	.attr("weight","5px")
	.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (r - x170 + 8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; });


});
});
