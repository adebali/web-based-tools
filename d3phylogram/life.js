var r = radius;

var cluster = d3.layout.cluster()
    .size([360, 1])
    .sort(null)
    .value(function(d) { return d.length; })
    .children(function(d) { return d.branchset; })
    .separation(function(a, b) { return 1; });

function project(d) {
  var r = d.y, a = (d.x - 90) / 180 * Math.PI;
  return [r * Math.cos(a), r * Math.sin(a)];
}

function cross(a, b) { return a[0] * b[1] - a[1] * b[0]; }
function dot(a, b) { return a[0] * b[0] + a[1] * b[1]; }

function step(d) {
  var s = project(d.source),
      m = project({x: d.target.x, y: d.source.y}),
      t = project(d.target),
      r = d.source.y,
      sweep = d.target.x > d.source.x ? 1 : 0;
  return (
    "M" + s[0] + "," + s[1] +
    "A" + r + "," + r + " 0 0," + sweep + " " + m[0] + "," + m[1] +
    "L" + t[0] + "," + t[1]);
}

var wrap = d3.select("#vis").append("svg")
    .attr("width", r * 2)
    .attr("height", r * 2)
    .style("-webkit-backface-visibility", "hidden");

// Catch mouse events in Safari.
wrap.append("rect")
    .attr("width", r * 2)
    .attr("height", r * 2)
    .attr("fill", "none")

var vis = wrap.append("g")
    .attr("transform", "translate(" + r + "," + r + ")");

var start = null,
    rotate = 0,
    div = document.getElementById("vis");

function mouse(e) {
  return [
    e.pageX - div.offsetLeft - r,
    e.pageY - div.offsetTop - r
  ];
}

wrap.on("mousedown", function() {
  wrap.style("cursor", "move");
  start = mouse(d3.event);
  d3.event.preventDefault();
});
d3.select(window)
  .on("mouseup", function() {
    if (start) {
      wrap.style("cursor", "auto");
      var m = mouse(d3.event);
      var delta = Math.atan2(cross(start, m), dot(start, m)) * 180 / Math.PI;
      rotate += delta;
      if (rotate > 360) rotate %= 360;
      else if (rotate < 0) rotate = (360 + rotate) % 360;
      start = null;
      wrap.style("-webkit-transform", null);
      vis
          .attr("transform", "translate(" + r + "," + r + ")rotate(" + rotate + ")")
        .selectAll("text")
          .attr("text-anchor", function(d) { return (d.x + rotate) % 360 < 180 ? "start" : "end"; })
          .attr("transform", function(d) {
            return "rotate(" + (d.x - 90) + ")translate(" + (r - 170 + 8) + ")rotate(" + ((d.x + rotate) % 360 < 180 ? 0 : 180) + ")";
          });
    }
  })
  .on("mousemove", function() {
    if (start) {
      var m = mouse(d3.event);
      var delta = Math.atan2(cross(start, m), dot(start, m)) * 180 / Math.PI;
      wrap.style("-webkit-transform", "rotateZ(" + delta + "deg)");
    }
  });

var length2pxConstant = 115;

function phylo(n, offset) {
  //if (n.length != null) offset += n.length * 115;
  if (n.length != null) offset += n.length * length2pxConstant;
  n.y = offset;
  if (n.children)
    n.children.forEach(function(n) {
      phylo(n, offset);
    });
}


function nodeDistance(node){
	if (node.parent){
	var distance = node.length;
	theParent = node.parent;
	while(theParent.parent){
//		console.log("HEEYYYY");
		if(theParent.length){
		distance += theParent.length;
			}
		theParent = theParent.parent;
		}
	return distance;
	}
	else{
//		console.log("No parent")
		return 0;
		}
	}

d3.text(nwkFile, function(text) {
  var x = newick.parse(text);
  var nodes = cluster.nodes(x);
  phylo(nodes[0], 0);

console.log(nodes);
  var link = vis.selectAll("path.link")
      .data(cluster.links(nodes))
    .enter().append("path")
      .attr("class", "link")
      .attr("d", step);

  var node = vis.selectAll("g.node")
      .data(nodes.filter(function(n) { return n.x !== undefined; }))
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

  node.append("svg:circle")
        .attr('stroke',  function(d){return d.children ? 'black':'yellowGreen';})
        .attr('fill', function(d){return d.children ? 'gray':'greenYellow';})
        .attr('stroke-width', function(d){return d.children ? "0.5px":"3px";})
      	.attr("r",function(d){return d.children ? 1.5:2.5;});

  node.append("svg:title")
      //.text(function(d){return d.name;});
//      .text(function(d){return d.dx;});
//      .text(function(d){return nodeDistance(d);});
      .text(function(d){return nodeDistance(d);});


  node.append("svg:circle")
	//.attr("dy","2px")
	.attr("cx", function(d){return r*3.24/4-length2pxConstant*nodeDistance(d);})
//	.attr("dx", function(d){return d.dx + 5;})
	//.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (r - 170 + 8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; })
        .attr('stroke',  function(d){return d.children ? 'black':'red';})
//        .attr('fill', function(d){return d.children ? 'gray':'white';})
        .attr('stroke-width', function(d){return d.children ? "0px":"3px";})
      	.attr("r",function(d){return d.children ? 0:1.5;});
//      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; });

  var label = vis.selectAll("text")
      .data(nodes.filter(function(d) { return d.x !== undefined && !d.children; }))
    .enter().append("text")
      .attr("y", ".31em")
      .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (r - 170 + 8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; })
      .text(function(d) { return d.name.replace(/_/g, ' '); });

  var marker = vis.selectAll("svg")
	.data(nodes.filter(function(d) { return d.x !== undefined && !d.children; }))
	.enter().append("rect")
	.attr("y", ".31em")

//	.attr("x", ".31em")
	.attr("height","5px")
	.attr("weight","5px")
	.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (r - 170 + 8) + ")rotate(" + (d.x < 180 ? 0 : 180) + ")"; });


});
