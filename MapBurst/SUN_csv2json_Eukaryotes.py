from pymongo import MongoClient
import json
import sys
from glob import glob
from pprint import pprint

if '--print' in sys.argv:
	PRINT = 1
else:
	PRINT = 0

def cprint(text):
	if PRINT==1:
		print(text)

sys.path.append('/home/ogun/.local/lib/python3.2/site-packages')


helpLine = ""
helpLine += "This script takes CSV file and outputs 2 json files.\n\n"
helpLine += "usage: python3 SUN_csv2json.py -m input.csv\n"
helpLine += "outputs: input.json input.noded.json\n"
helpLine += "input.noded.json will be what you need for SunBurst drawing\n\n"
helpLine += "Options:\n"
helpLine += "\t-m your_csv_input (This should have an header as 'taxid,flag1,flag2...')\n"
helpLine += "\t-b background.txt (Optional: This file will have taxid list (each at a line) of your background.\n"
helpLine += "\t--mb (Optional: Use CSV input as a background.)\n"
helpLine += "\tif no -b or --mb entered, the default background will be used.\n"
helpLine += "\t-batch yourWildcard (eg *csv)  (not compatible with -m, and used for batch requests)\n"
helpLine += "\t--num Use that if you don't want binary json, but numeric\n"

if '-h' in sys.argv:
	print(helpLine)
	sys.exit()

nameCol = MongoClient()["taxonomy"]["name"]
missingCol = MongoClient()["taxonomy"]["missing"]
ArcCol = MongoClient()["MapBurst"]["Arc"]

root = "/home/ogun/public_html/MapBurst/"

markfilelist = []
useDB = 0

if '-batch' in sys.argv:
        markinput = 1
        wildcard = sys.argv[sys.argv.index('-batch') + 1] 
        markfilelist = glob(wildcard) 
        #markfilelist = glob("*csv") 
        #print(markfilelist) 
elif '-m' in sys.argv:
	markinput = 1
	markfile = sys.argv[sys.argv.index('-m') + 1]
	markfilelist.append(markfile)
elif '-d' in sys.argv:
	useDB = 1	
	domainText = sys.argv[sys.argv.index('-d') + 1]
	domainList = domainText.split('/')
	markfilelist.append(root + "temp.csv")
else:
	markinput = 0
	print("No markfile or batch wildcard specified, just background will be plotted!")
        #sys.exit()

if '-b' in sys.argv:
	defaultbg = 0
	backgroundfile = sys.argv[sys.argv.index('-b') + 1]
	bglist = []
	filein = open(backgroundfile,'r')
	for line in filein:
		if line.strip():
			bglist.append(int(line.strip()))
	print(len(bglist))
elif '--mb' in sys.argv:
	defaultbg = 0
	bglist = []
	for file in markfilelist:
		#print(file)
		filein = open(file,"r")
		filein.readline()
		for line in filein:
			#print(line)
			taxid = int(line.split(',')[0])
			if not taxid in bglist:
				print(taxid)
				bglist.append(taxid)
	print('Background list size is ',len(bglist))
else:
	defaultbg = 1
	cprint("Default background list will be used.")

if '--num' in sys.argv:
	binary = 0
else:
	binary = 1

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value



def record2supergroup(record):	

       # record = namecol.find_one({"_id":taxid})

        if record == None:
                newTaxidRecord = missingCol.find_one({"_id":taxid})
                if newTaxidRecord != None:
                        newTaxid = newTaxidRecord["newid"]
                        record = namecol.find_one({"_id":newTaxid})
                else:
			print("NA 783")
                        return "NA"
        if record == None:
                print ("No record found for:")
                print(taxid)
                sys.exit()
	print(record)

        lineage = record["lineage"]
        ll = list(filter(None,lineage.split(';')))
        superkingdom = record["nat"]["d"]
        if superkingdom == "":
                if ll[-1] == "unclassified sequences":
                        return "unclassified","unclassified"
                if ll[-1] == "other sequences":
                        return ll[-1],ll[-2]
                else:
                        print(lineage)
                        print ("No superkingdom found for " + str(taxid))
                        sys.exit()



        EukSuper = {
                #       "Pelagophyceae"         :"Chromalveolates",
                #       "Metazoa"               :"Opisthokonts",
                        "Viridiplantae"         :"Plantae",
                        "Rhodophyta"            :"Plantae",
                        "Glaucocystophyceae"    :"Plantae",
                        "Kinetoplastida"        :"Excavates",
                #       "Dictyosteliida"        :"Amoebozoa",
                        "Heterolobosea"         :"Excavates",
                        "Fornicata"         :"Excavates",
                #       "Ichthyosporea"         :"Opisthokonts",
                #       "Choanoflagellida"      :"Opisthokonts",
                        "Entamoeba"             :"Amoebozoa",
                        "Stramenopiles"         :"Chromalveolates",
                        "Haptophyceae"          :"Chromalveolates",
                        "Alveolata"             :"Chromalveolates",
                        "Cryptophyta"           :"Chromalveolates",
                        "Euglenozoa"            :"Excavates",
                        "Parabasalia"           :"Excavates",
			""			:""
                }



        if superkingdom == "Eukaryota":
                if "Opisthokonta" in lineage:
                        supergroup = "Opisthokonts"
                elif "Amoebozoa" in lineage:
                        supergroup = "Amoebozoa"
                elif "Rhizaria" in lineage:
                        supergroup = "Rhizaria"
                elif ll[-2] in EukSuper.keys():
                        supergroup = EukSuper[ll[-2]]
                else:
			supergroup = "NA"
                        print("No Eukaryotic Supergroup found!")
                        print(lineage)
                        #sys.exit()
                return supergroup
        elif superkingdom == "Archaea" or superkingdom == "Bacteria" or superkingdom == "Viruses":
                return ""
        else:
                cprint("No even superkingdom found for " + str(taxid))
                cprint(lineage)
                sys.exit()


def taxid2treeList(taxid):
	record = nameCol.find_one({"_id":taxid})
	#record = nameCol.find_one({"_id":351605})
	#print("RECORD: ",record)
	if record == None:
		#print('-->',taxid,",",record)
		newTaxid = missingCol.find_one({"_id":taxid})["newid"]
		record = nameCol.find_one({"_id":newTaxid})	


	nat = record["nat"]
	thelist = []
	thelist.append(nat["d"])
	thelist.append(record2supergroup(record))
	thelist.append(nat["k"])
	thelist.append(nat["p"])
	thelist.append(nat["c"])
	thelist.append(nat["o"])
	thelist.append(nat["f"])
	thelist.append(nat["g"])
	thelist.append(nat["s"])
	stringlist = []
	for element in thelist:
		stringlist.append(str(element))
	return stringlist

#def taxidList2treeDict(taxid_list):

if defaultbg ==1:
	bglist=[]
	bgrecords = nameCol.find({"ass":1,"nat.s":{"$ne":""},"nat.d":"Eukaryota"})
	total = bgrecords.count()
	print(total)
	for i in range(total):
		taxid = next(bgrecords)["_id"]
		bglist.append(taxid)
	#BgCol = MongoClient()["MapBurst"]["bg"]
	#Bg = BgCol.find_one()
	#bglist = Bg["organisms"]
	total = len(bglist)
	print(bglist)
else:
	total = len(bglist)
	#print(total)

if useDB ==1:
	temp = open(root + "temp.csv","w")
	temp.write("taxid")
	for domain in domainList:
		temp.write("," + domain)
	temp.write("\n")
	for taxid in bglist:
		temp.write(str(taxid))
		for domain in domainList:
			r = ArcCol.find_one({"_id":taxid},{domain:1})
			if domain in r.keys():
				flag = r[domain]
			else:
				flag = 0
			temp.write("," + str(flag))
		temp.write("\n")	
	temp.close()

def createFirstJSON(markfile):
	print('createFirstJSON')
	firstJSON = markfile + ".json"

	tree = AutoVivification()
	markdict = AutoVivification()
	marklist = []



	filein = open(markfile,'r')
	header = filein.readline()
	#print(markfile)
	hl = header.split(',')
	totalmarks = len(hl)-1
	mycount = 0
	for line in filein:
		mycount += 1
		#print(line.strip() + "-" + str(mycount))
		ll = line.split(',')
		taxid = int(ll[0])
		if taxid != 0:
			marklist.append(taxid)
		for i in range(1,totalmarks+1):
			if binary == 1:
				markdict[taxid][hl[i].strip()] = int(ll[i])
			else:
				if  markdict[taxid][hl[i].strip()]:
					markdict[taxid][hl[i].strip()].append(int(ll[i]))
				else:
					markdict[taxid][hl[i].strip()] = [int(ll[i])]

	#print(markdict)	
	#print(markdict[329726])	
	n = "name"
	c = "children"
	domainlist = []
	for i in range(1,len(hl)):
		domainlist.append(hl[i].strip())
	


#	domain = hl[1].strip()
	print(total)
	for i in range(total):

		#print(i)
		taxid = bglist[i]

		print(i,taxid)
		i += 1
		#print('taxid is ',taxid)
		l = taxid2treeList(taxid)
		print(l)
	#	print(l)
		#for taxid in taxid_list:
		#print(taxid)
		#proteincount = nameCol.find_one({"_id":taxid})

	#	if "NRcount" in proteincount:
	#		writenum = proteincount["NRcount"]
	#	else:
	#		writenum = 0
	#	print(writenum)
	#	tree["root"][l[0]][l[1]][l[2]][l[3]][l[4]][l[5]][l[6]][l[7]][l[8]+ "|" + str(taxid)][domain] = writenum

		for domain in domainlist:
			tree["root"][l[0]][l[1]][l[2]][l[3]][l[4]][l[5]][l[6]][l[7]][l[8]+ "|" + str(taxid)][domain] = 0

	#	tree[c].append({n:l[0],c:l[1].append({n:l[2],c:l[3].append({n:l[4],c:l[5].append({n:l[6],c:l[7].append({n:l[8],s:1})})})})})

	#print(markdict)
	#print(markdict[k606]["mydomain"])
	#for key in markdict:

	for pos_taxid in marklist:
		#print (pos_taxid)
	#	if key in bglist:
		theparent = 11111111111111111111
		pos_record = nameCol.find_one({"_id":pos_taxid})
		#if "hasParent" in list(pos_record.keys()):
		#	if pos_record["hasParent"]==1:
		#		theparent = pos_record["parent"]

		if pos_taxid in bglist:
			#print(pos_taxid)
			l = taxid2treeList(pos_taxid)
			for domain in domainlist:
				tree["root"][l[0]][l[1]][l[2]][l[3]][l[4]][l[5]][l[6]][l[7]][l[8]+ "|" + str(pos_taxid)][domain] = markdict[pos_taxid][domain]
			#tree["root"][l[0]][l[1]][l[2]][l[3]][l[4]][l[5]][l[6]][l[7]][l[8]+ "|" + str(pos_taxid)][domain] = nameCol.find_one({"_id":pos_taxid})["NRcount"] 
			#print (currentdict)
			#for element in markdict[key]:
			#	print(element)
			#	print(key)
			#	print(markdict[key][element])
			#	print(currentdict)
			#	currentdict[element] = markdict[key][element]
			#	currentdict = dict(currentdict[element])
		elif theparent!= pos_taxid and theparent in bglist:
			l = taxid2treeList(theparent)
			for domain in domainlist:
				tree["root"][l[0]][l[1]][l[2]][l[3]][l[4]][l[5]][l[6]][l[7]][l[8]+ "|" + str(theparent)][domain] = markdict[pos_taxid][domain]

	d = dict(tree)
	print(d)
#	myjson = json.dumps(d)
	myjson = json.dumps(d, indent=1)
	out = open(firstJSON,"w")
	out.write(myjson)
	print("MyJson:")
	print(myjson)

def textWiseAddNodes(file):
	
	#domain = file.split('.')[0].strip()
	def getDomainName(line):
		if ':' in line:
			return line.split(':')[0].strip()
		else:
			return None

	firstJSON = file + ".json"
	out = open(firstJSON.replace(".json",".noded.json"),'w')
	

	filein = open(firstJSON,'r')
	d = 0

	domains = []
	for line in filein:
		if line.startswith(' '*11 + '"'):
			d = 1
			domains.append(line.strip().split(":")[0].replace('"',''))
		elif (d==1):
			break
	cprint(domains)
	
	
	filein = open(firstJSON,'r')
	
	

	def getname(line):
		return line.split('"')[1]

	current = 0 
	newlines = []
	maxcurrent = 11

	for line in filein:
		skip = 0
		closecurly = 0
		old = current
		for k in range(1,maxcurrent+1):
			if line.startswith(' '*k + '"'):
				current = k
				skip = 0
				break
		else:
			skip = 1
			if '}' in line:
				closecurly = 1
	
		co = current - old
		oc = co*-1
		
		if (co)<0 and skip==0:
			#print(line)
			#print(co)
			#sys.exit()
			for i in range(oc-1):
				nocomma = newlines[-1].replace(",","").replace(' "size"',', "size"')
				del newlines[-1]
				newlines.append(nocomma)
				newlines.append(' '*(maxcurrent-i+1) + ']')
				newlines.append(' '*(maxcurrent-i-1) + '}')

			nocomma = newlines[-1].replace(",","").replace(' "size"',', "size"')
			del newlines[-1]
			newlines.append(nocomma)
			newlines.append(' '*(maxcurrent-i) + ']')
			newlines.append(' '*(maxcurrent-i-1) + '},')
			#newlines.append(' '*(maxcurrent-i+1) + ']')

		
		if skip == 0:
			if getDomainName(line) in domains:
				thevalue = int(line.split('"'+getDomainName(line)+'":')[1].strip())
			newlines.append(' '*current + '{')
			if current == maxcurrent+1:
				newlines.append(' '*current + '"name": "' + getname(line) + '", "size": ' + str(thevalue) + '},')
			else:
				newlines.append(' '*current + '"name": "' + getname(line) + '",')	

			if current!=maxcurrent:
				newlines.append(' '*(current) + '"children": [')
	#print(newlines)	
	newlines[-1] = newlines[-1].replace(",","").replace(' "size"',', "size"')
	for i in range(maxcurrent-1):
		newlines.append("]}")

	for line in newlines:
		out.write(line + "\n")



def objectWiseAddNodes(file):
	n = "name"
	c = "children"
	#domain = file.split('.')[0].strip()
	def getDomainName(line):
		if ':' in line:
			return line.split(':')[0].strip()
		else:
			return None

	firstJSON = file + ".json"
	out = open(firstJSON.replace(".json",".noded.json"),'w')
	

	filein = open(firstJSON)
	d = 0

	domains = []
	for line in filein:
		if line.startswith(' '*11 + '"'):
			d = 1
			domains.append(line.strip().split(":")[0].replace('"',''))
		elif (d==1):
			break
	#print(domains)
	
	
	filein = open(firstJSON)
	mydict = json.load(filein)
	#print (mydict)
	newtree = AutoVivification()
	for key in mydict:
		#print(key)
		newtree[n] = key
		newtree[c] = []
		key2index = 0
		for key2 in mydict[key]:
			newtree[c].append({n:key2,c:[]})
			#print(key2)
			key3index = 0
			for key3 in mydict[key][key2]:
				#print(key3)
				newtree[c][key2index][c].append({n:key3,c:[]})
				key4index = 0
				for key4 in mydict[key][key2][key3]:
					newtree[c][key2index][c][key3index][c].append({n:key4,c:[]})
					key5index = 0
					for key5 in mydict[key][key2][key3][key4]:
						newtree[c][key2index][c][key3index][c][key4index][c].append({n:key5,c:[]})
						key6index = 0
						for key6 in mydict[key][key2][key3][key4][key5]:
							newtree[c][key2index][c][key3index][c][key4index][c][key5index][c].append({n:key6,c:[]})
							key7index = 0
							for key7 in mydict[key][key2][key3][key4][key5][key6]:
								newtree[c][key2index][c][key3index][c][key4index][c][key5index][c][key6index][c].append({n:key7,c:[]})
								key8index = 0
								for key8 in mydict[key][key2][key3][key4][key5][key6][key7]:
									newtree[c][key2index][c][key3index][c][key4index][c][key5index][c][key6index][c][key7index][c].append({n:key8,c:[]})
									key9index = 0	
									for key9 in mydict[key][key2][key3][key4][key5][key6][key7][key8]:
										newtree[c][key2index][c][key3index][c][key4index][c][key5index][c][key6index][c][key7index][c][key8index][c].append({n:key9,c:[]})
										key10index = 0
										for key10 in mydict[key][key2][key3][key4][key5][key6][key7][key8][key9]:
											domainobject = mydict[key][key2][key3][key4][key5][key6][key7][key8][key9][key10]
											lastdict = {}
											lastdict[n] = key10
											i = 1
											for domain in domains:
												lastdict['d'+str(i)]={}
												lastdict['d'+str(i)][n] = domain
												lastdict['d'+str(i)]['exist'] = mydict[key][key2][key3][key4][key5][key6][key7][key8][key9][key10][domain]
												i +=1

											newtree[c][key2index][c][key3index][c][key4index][c][key5index][c][key6index][c][key7index][c][key8index][c][key9index][c].append(lastdict)
			#								print(mydict[key][key2][key3][key4][key5][key6][key7][key8][key9][key10])
											key10index += 1
										key9index += 1
									key8index += 1
								key7index += 1
							key6index += 1
						key5index += 1
					key4index += 1
				key3index += 1
			key2index += 1






		
	myjson = json.dumps(dict(newtree), sort_keys=True, indent= 1)

	out.write(myjson)
	

for markfile in markfilelist:
	createFirstJSON(markfile)
	objectWiseAddNodes(markfile)
