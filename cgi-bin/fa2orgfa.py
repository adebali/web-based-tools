import os
import sys



def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order
	

inputfile = sys.argv[1]
extension = '.' + inputfile.split('.')[-1]
newextension = '.org' + extension
try:
	outputfile = sys.argv[2]
except:
	outputfile = inputfile.replace(extension,newextension)
outfile = open(outputfile,'w')
d,l = fastareader(inputfile)


o = {}
for header in l:
	try:
		organism = header.split('[')[1].split(']')[0].strip().replace(' ','_')
	except:
		organism = 'undefined'
	try:
		o[organism] += 1
	except:
		o[organism] = 1
#	new_header = organism + "_" + str(o[organism]) + " |" + header.split('[')[0].strip()
	new_header = organism + "_" + str(o[organism]) + " |" + header
	outfile.write(">" + new_header + "\n")
	outfile.write(d[header] + "\n")
