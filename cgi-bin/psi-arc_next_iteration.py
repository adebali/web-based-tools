#!/usr/bin/python

# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time
from glob import glob
import re

link = "http://leonidas.bio.utk.edu/psi-arc_results.html"

print "Content-type: text/html\n\n" 
print '''<meta http-equiv="refresh" content="1;url='''+link+'''" />'''

#maxSeqNum = 500

form = cgi.FieldStorage() 
# Get data from fields
#fileitem = form['BROWSED']
#sequence = form.getvalue('SEQUENCE')

def displayError(status):
        htmltext = '''
                <html>  <head><title>CoDoST Error</title></head>
                        <link rel="stylesheet" type="text/css" href="http://web.utk.edu/~oadebali/codost_style.css">
			<body>
			<p id="Ztitle">CoDoST Error!</p>
			<p id="Zh2">--> 
                '''
        htmltext += status
        htmltext += '</p></body></html>'
        print htmltext
	sys.exit()

def skimInput(inputfile):
	code = "grep -c '>' " + inputfile
	#print code
        numberOfGT = int(os.popen(code).read())
        if numberOfGT > maxSeqNum:
                status = 0
        else:
                status = 1 

        if status == 0:
                return "Error 662 Incorrect FASTA format or sequence number exceeds the maximum limit of " + str(maxSeqNum)
        elif status == 1:
                return "PASS"

def fastaReaderCorrecter(datafile, just_name = 'no'):
        data = open(datafile,'r')
        seq_dic = {}
        list_order = []
	s = ''
	max_seq_length = 1
        for line in data:
                line = line.replace('\r','')
                if line[0] == '>':
                        if just_name == 'Yes':
                                name = line.split('/')[0][1:]
                        else:
                                name = line[1:-1]
				if any(name == s for s in list_order) or name == '':
					return list_order, dataout,"Duplicated or void sequence header"
                        list_order.append(name)
                        seq_dic[name] = ''
                else:
                        while line.find('\n') != -1:
                                line = line[0:line.find('\n')] + line[line.find('\n')+2:]
                        seq_dic[name]= seq_dic[name] + re.sub(r'-|x|X|b|B|j|J|o|O|x|X|z|Z|\d|\W,','',line)
			max_seq_length = max(max_seq_length,len(seq_dic[name]))
        dataout = ''
        for k, v in seq_dic.iteritems():
                dataout = dataout + '>' + k + '\n' + v + '\n'

        data.close()
	s = "PASS"
        return list_order, dataout, s, max_seq_length


def checkFASTAformat(inputfile):
        dataout = ''
        try:
                l, dataout, s, max_seq_length = fastaReaderCorrecter(inputfile)
		if s != "PASS":
			displayError(s)
        except:
                displayError(" incorrect FASTA format")

        if len(l) > maxSeqNum:
                displayError("Sequence number exceeds the maximum limit of " + str(maxSeqNum))
        
	return dataout, max_seq_length



def qualityControl(inputfile,outputfile):
	outfile = open(outputfile,'w')
	status1 = skimInput(inputfile)
	if status1 == "PASS":
		dataout, max_seq_length = checkFASTAformat(inputfile)
		
		#print outputfile
		outfile.write(dataout)
		outfile.close()
		return max_seq_length
	else:
        	displayError(status1)


def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return


def thereisnoinput():
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "<title>CoDoST</title>"
	print "</head>"
	print "<body>"
	print "There is no input!"
	print "</body>"
	print "</html>"


def intheprocesspage(outhtml):
	filename='../htdocs/codost_out/'+outhtml
	#filename='process.html'
	web=open(filename,'w')
	text=''' 
	<html>
	<head>
	<title>CoDoST</title>
	<meta http-equiv="refresh" content="5" >
	</head>
	<body>
	Your work is being processed. Please be patient
	</body>
	</html>
	'''
	web.write(text)
	web.close()
	return 1


def timeDiff(t):
	def t2sec(timestring):
		return int(timestring[4:6])*30*24*24*60 + int(timestring[6:8])*24*24*60 + int(timestring[8:10])*24*60 + int(timestring[10:12])*60 + int(timestring[12:14])
	currenttime = t2sec(str(datetime.datetime.now()).replace(" ","").replace("-","").replace(":","").replace(".",""))
	oldtime = t2sec(t)
	diff = currenttime - oldtime
	return diff
	

	
def generateparametersline(databasetext,flags):
	parametersline=' --print -cpu 1 --nodraw -d '+databasetext+' '+flags
	return parametersline
	

def printFirstMoment(link):

	print'''
	<html>
		<head>
			<title>CoDoST</title>
			<meta http-equiv="refresh" content="1;url='''+link+'''" />	
                        <link rel="stylesheet" type="text/css" href="http://web.utk.edu/~oadebali/codost_style.css">
			</head>
		
		<body onload=newDoc()>
			<p id="Ztitle">CoDoST</p>
			<p id="Zh2">Your input sequences look good</p>
			<p id="Zh2">You can either wait or save this link and go to your results after some time:</p>
			<p id="Zh2"><a href='''+link+''' target=\"_blank\">'''+link+'''</a></p>

			<p id="Zh2">You will be directed to this page in a moment</p>
		</body>
	</html>'''


# Generate a UNIQUE ID
#mytime=datetime.datetime.now()
#t=str(mytime).replace(" ","").replace("-","").replace(":","").replace(".","")
#outhtml=t+".html"

#link= "http://leonidas.bio.utk.edu/codost_out/"+t+".html"

# Create instance of FieldStorage 


# Define the locations and names for inputs
#tempfilename = '../htdocs/TEMP/TEMP'+t+'.fa'
#fastafilename='../htdocs/requests/FASTA'+t+'.fa'
#permanentfastafilename='../htdocs/FASTA/FASTA'+t+'.fa'
#paramfilename = parametersfilename='../htdocs/requests/PARAM'+t+'.txt'
#permanentparamfilename = '../htdocs/PARAM/PARAM'+t+'.txt'

# Save the fasta file of user input
#textbox=0
#cpfasta = "cp " + fastafilename + " " + permanentfastafilename
#cpparam = "cp " + paramfilename + " " + permanentparamfilename



#print "Content-type:text/html \r\nContent-Length: 348\r\n\r\n"

outfile=open("psi-arc_negative_list.txt",'a')
submittedfile = open("psi-arc_submitted_gi.txt","r");
submitted = []
for line in submittedfile:
	submitted.append(line.strip())


for gi in submitted:
	value = form.getvalue(gi)
	print value,'\n'
	if value != "True":
		outfile.write(gi + "\n")
	else:
		pass







#Write parameters file

outfile.close()

def waitPage(t):
	# Open the Pre-Results Page

	filepath="/var/www/localhost/htdocs/codost_out/"+t+".html"
	#print filepath
	preresultf=open(filepath,'w')
	preresultf.write('''<html>
		<head>
			<title>CoDoST-Processing</title>
			<LINK REL="SHORTCUT ICON"  HREF="http://web.utk.edu/~oadebali/waiting.ico">
			<meta http-equiv="content-type" content="text/html; charset=UTF-8">
			<meta http-equiv="refresh" content="60">

		</head>
	
		<body>
			<h2>CoDoST</h2>
				<p>Your results will be here when ready!</p>
				<p>This page will be refreshed every minute.</p>
				<p></p>
				<p>Workflow example:</p>
				<img src="http://web.utk.edu/~oadebali/oksuzlar.gif" width="500" height="300"/>
		</body>
	</html>''')
	preresultf.close()

#waitPage(t)
#intheprocesspage(t)

code = "rm ../htdocs/psi-arc_adjusted_blastout.csv"
os.system(code)

code = '/home/ogun/tools_o/ncbi-blast-2.2.28+/bin/psiblast -in_pssm psi-arc_PSSM.pssm -db /home/ogun/9606/9606-giGID.fa -out_pssm psi-arc_PSSM.pssm -out psi-arc_blastout.txt -num_descriptions 1000 -evalue 1 -num_iterations 2 -negative_gilist psi-arc_negative_list.txt'
os.system(code)

output = open("../htdocs/psi-arc_adjusted_blastout.csv","w")
negative_list = open("psi-arc_negative_list.txt","a")
output.write("Description,Score,E-value\n")
geneIDlist = []
newhit = []
submitted = []
firstsubmitted = open("psi-arc_submitted_gi.txt","r")



for line in firstsubmitted:
	submitted.append(line.strip())

firstsubmitted.close()

openblastout = open("psi-arc_blastout.txt","r")
lines = openblastout.readlines()

c=0

for line in lines:
        if "Sequences producing significant alignments:" in line:
                myindex=c+1
                break
        else:
                c+=2

for i in range(myindex, len(lines)):
        print lines[i]
        if lines[i].strip() == "":
                endindex=i
                break

submittedfile = open("psi-arc_submitted_gi.txt","a")

for i in range(myindex,endindex):
        line = lines[i].replace(",","")
        description = line[0:68]

        if "|gi" in description:
                gi = int(description.split("|gi")[1].split(" ")[0])
        else:
                gi = 0


        if "geneID|" in description:
                geneid = int(description.split("geneID|")[1].split('|')[0])
        else:
                geneid = 0

        if geneid != 0 and geneid not in geneIDlist:
                score = line[69:75]
                evalue = line[76:]
                newline = description + "," + score + "," + evalue
                output.write(newline)
                geneIDlist.append(geneid)
		if gi not in submitted:
                	submittedfile.write(str(gi) + "\n")
			newhit.append(gi)
        else:
                negative_list.write(str(gi) + "\n")
output.close()
submittedfile.close()

newhitfile = open("../htdocs/newhit.txt","w")
for gi in newhit:
	newhitfile.write(gi + "\n")
newhitfile.close()
