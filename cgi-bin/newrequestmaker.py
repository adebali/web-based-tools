#!/usr/bin/python

# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time


def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return




def thereisnoinput():
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "<title>CoDoST</title>"
	print "</head>"
	print "<body>"
	print "There is no input!"
	print "</body>"
	print "</html>"


def intheprocesspage(outhtml):
	filename='../htdocs/codost_out/'+outhtml
	#filename='process.html'
	web=open(filename,'w')
	text=''' 
	<html>
	<head>
	<title>CoDoST</title>
	<meta http-equiv="refresh" content="5" >
	</head>
	<body>
	Your work is being processed. Please be patient
	</body>
	</html>
	'''
	web.write(text)
	web.close()
	return 1

	
def generateparametersline(databasetext,flags):
	parametersline=' --print -cpu 1 --nodraw -d '+databasetext+' '+flags
	return parametersline
	

# Generate a UNIQUE ID
mytime=datetime.datetime.now()
t=str(mytime).replace(" ","").replace("-","").replace(":","").replace(".","")
outhtml=t+".html"

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
fileitem = form['BROWSED']
sequence = form.getvalue('SEQUENCE')

# Define the locations and names for inputs
fastafilename='../htdocs/requests/FASTA'+t+'.fa'
parametersfilename='../htdocs/requests/PARAM'+t+'.txt'

# Save the fasta file of user input
textbox=0
if fileitem.filename:
	open(fastafilename, 'wb').write(fileitem.file.read())
elif sequence:
	textbox=1
	fas=open(fastafilename,'w')
	fas.write(sequence)
	fas.close()
else:
	thereisnoinput()

flags=' -newton '+t+' '

hhblitsdb=form.getvalue("bdatabase")
if hhblitsdb != 'None':
	hhblitsprobability=form.getvalue("blitsprob")
	flags=flags+' -b '+hhblitsdb+'_'+hhblitsprobability+' '

# Work on the parameters
databasetext=''
runpfamscan=form.getvalue("pfamscan")
if runpfamscan:
	runhmmer=1
else:
	runhmmer=0
	runpfamscan="False"
	
rundomainsplit=form.getvalue("domainsplit")
if rundomainsplit:
	domainsplit=1
	domainsplitpercentage=float(form.getvalue("domainsplitpercentage"))
	flags=flags+' -splitdomain '+str(domainsplitpercentage)+' '
else:
	domainsplit=0
	domainsplitpercentage=100.0;


tmprediction=form.getvalue("TM")
if tmprediction is "tmhmm":
	TMmethod="tmhmm"
	runTM=1
elif tmprediction is "phobius":
	TMmethod="phobius"
	runTM=1
else:
	TMmethod="tmhmm"
	runTM=1

if runhmmer==0:
	flags=flags+' --nohmmer'
	
if runTM==0:
	flags=flags+' --noTM'
elif runTM==1:
	flags=flags+' -tm '+TMmethod

for i in range(1,6):
	db="database"+str(i)
	db=form.getvalue(db)
	prob="prob"+str(i)
	prob=form.getvalue(prob)
	gap="gap"+str(i)
	gap=form.getvalue(gap)
	databasetext=databasetext+str(db)+'_'+str(prob)+'_'+str(gap)+'AND'

databasetext=databasetext[:-3]

parameters=generateparametersline(databasetext,flags)

#Write parameters file
pfile=open(parametersfilename,'w')
pfile.write(parameters)
pfile.close()


# Open the Pre-Results Page

filepath="../htdocs/codost_out/"+outhtml
preresultf=open(filepath,'w')
preresultf.write('''<html>
	<head>
		<title>CoDoST-Processing</title>
		<LINK REL="SHORTCUT ICON"  HREF="http://web.utk.edu/~oadebali/waiting.ico">
		<meta http-equiv="refresh" content="60">

	</head>
	
	<body>
		<h2>CoDoST</h2>
			<p>Your results will be here when ready!</p>
			<p>This page will be refreshed every minute.</p>
			<p></p>
			<p>Workflow example:</p>
			<img src="http://web.utk.edu/~oadebali/oksuzlar.gif" width="500" height="300"/>
	</body>
</html>''')
preresultf.close()

# Define the future output link
link="http://leonidas.bio.utk.edu/codost_out/"+outhtml



# Write an html as an interface

print "Content-type:text/html \r\nContent-Length: 348\r\n\r\n"
print'''
<html>
	<head>
		<title>CoDoST</title>
	<meta http-equiv="refresh" content="0;url='''+link+'''" />
		</head>
	
	<body onload=newDoc()>
		<h2>CoDoST</h2>
			<p>You can either wait or save this link and go to your results after some time:</p>
			<p><a href='''+link+''' target=\"_blank\">'''+link+'''</a></p>
	</body>
</html>'''
	
