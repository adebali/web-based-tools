#!/usr/bin/python

# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time



mytime=datetime.datetime.now()

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
sequence = form.getvalue('SEQUENCE')
fileitem = form['BROWSED']

textbox=0
if fileitem.filename:
	open('../htdocs/codost_in/fastainput.fa', 'wb').write(fileitem.file.read())
else:
	textbox=1

def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return


def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order	



def thereisnoinput():
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "<title>CoDoST</title>"
	print "</head>"
	print "<body>"
	print "There is no input!"
	print "</body>"
	print "</html>"


def intheprocesspage(outhtml):
	filename='../htdocs/codost_out/'+outhtml
	#filename='process.html'
	web=open(filename,'w')
	text=''' 
	<html>
	<head>
	<title>CoDoST</title>
	<meta http-equiv="refresh" content="5" >
	</head>
	<body>
	Your work is being processed. Please be patient
	</body>
	</html>
	'''
	web.write(text)
	web.close()
	return 1

	
def codost(input,databasetext,flags):
	if os.path.isfile('../htdocs/codost_out/sign.txt'):
		os.system('rm ../htdocs/codost_out/sign.txt')
	code1a='python /home/ogun/scripts/CoDoST1.51.py --nodraw -i '+input+' -d '+databasetext+' '+flags
	code1b=' && cp ../htdocs/codost_out/sig.txt ../htdocs/codost_out/sign.txt'
	code1='nohup '+code1a+code1b

	os.popen(code1)
	signpresent=os.path.isfile('../htdocs/codost_out/sign.txt')
	while not signpresent:
		print '<p>Z</p>'
		time.sleep(5)
	
def table2domfigure(outhtml):
	code2='python /home/ogun/scripts/table2domfigure152.py -i ../htdocs/codost_in/fastainput.fa -o ../htdocs/codost_out/'+outhtml
	code3=" && mv *.table ../htdocs/codost_out/"
	code5=code2+code3
	os.popen(code5)
	


text=''
runpfamscan=form.getvalue("pfamscan")
if runpfamscan:
	runhmmer=1
else:
	runhmmer=0
	runpfamscan="False"
	
	
tmprediction=form.getvalue("TM")
if tmprediction is "tmhmm":
	TMmethod="tmhmm"
	runTM=1
elif tmprediction is "phobius":
	TMmethod="phobius"
	runTM=1
else:
	TMmethod="tmhmm"
	runTM=1


for i in range(1,6):
	db="database"+str(i)
	db=form.getvalue(db)
	prob="prob"+str(i)
	prob=form.getvalue(prob)
	gap="gap"+str(i)
	gap=form.getvalue(gap)
	text=text+str(db)+'_'+str(prob)+'_'+str(gap)+'AND'

text=text[:-3]
t=str(mytime).replace(" ","").replace("-","").replace(":","").replace(".","")
outhtml=t+".html"
filename='http://leonidas.bio.utk.edu/codost_out/'+outhtml



print "Content-type:text/html \r\nContent-Length: 348\r\n\r\n"
print "<html>"
print "<head>"
print "<title>CoDoST"
print "</title>"
print '''<script>
function newDoc()
{
alert("Your results are ready! Your html will be safe with us for a week: '''+runpfamscan+'''");
window.location.assign("'''+filename+'''");
}

</script>'''
link="http://leonidas.bio.utk.edu/codost_out/"+outhtml

print "</head>"
print '<body onload=newDoc()>'
print "<h2>CoDoST</h2>"
print "<p>We are working on your request...</p>"
print "<p>After we are done, you will be directed to results page.</p>"
print "<p></p>"
print "<p>Be patient... It takes 2-3 minutes for each sequence to scan 3 databases</p>"
print "<p>Click <a href="+link+" target=\"_blank\">HERE</a> for results!</p>"
print "<p>You can either wait or save this link and go to your results after some time:</p>"
print "<p><a href="+link+" target=\"_blank\">"+link+"</a></p>"


removefile("../htdocs/codost_out/detailed_output.txt")

flags=''
if runhmmer==0:
	flags=flags+' --nohmmer'
	
if runTM==0:
	flags=flags+' --noTM'
elif runTM==1:
	flags=flags+' -tm '+TMmethod


if not textbox:
	intheprocesspage(outhtml)
	d,l=fastareader("../htdocs/codost_in/fastainput.fa")
	for header in l:
		tem=open("../htdocs/codost_in/temporary.fa",'w')
		tem.write('>')
		tem.write(header)
		tem.write("\n")
		tem.write(d[header])
		tem.close()
		print "<p>"+header+"</p>"
		codost("../htdocs/codost_in/temporary.fa",text,flags)
		code="cat detailed_output.txt >>../htdocs/codost_out/detailed_output.txt"
		os.system(code)
	
	table2domfigure(outhtml)	
elif sequence and textbox:	
	intheprocesspage(outhtml)
	fas=open("../htdocs/codost_in/fastainput.fa",'w')
	fas.write(sequence)
	fas.close()
	
	d,l=fastareader("../htdocs/codost_in/fastainput.fa")
	for header in l:
		tem=open("../htdocs/codost_in/temporary.fa",'w')
		tem.write('>')
		tem.write(header)
		tem.write("\n")
		tem.write(d[header])
		tem.close()
		print "<p>"+header+"</p>"
		codost("../htdocs/codost_in/temporary.fa",text,flags)
		code="cat detailed_output.txt >>../htdocs/codost_out/detailed_output.txt"
		os.system(code)
	
	table2domfigure(outhtml)
else:
	thereisnoinput()

	
	
print "</body>"
print "</html>"
	