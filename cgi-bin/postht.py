#!/usr/bin/python

# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time

dtfile=open('datatransfer.txt','r')
lines=dtfile.readlines()
text=lines[0].replace('\n','')
outhtml=lines[1].replace('\n','')
filename=lines[2].replace('\n','')
textbox=lines[3].replace('\n','')
dtfile.close()


def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order	



def thereisnoinput():
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "<title>CoDoST</title>"
	print "</head>"
	print "<body>"
	print "There is no input!"
	print "</body>"
	print "</html>"


def intheprocesspage(outhtml):
	filename='../htdocs/codost_out/'+outhtml
	#filename='process.html'
	web=open(filename,'w')
	text=''' 
	<html>
	<head>
	<title>CoDoST</title>
	<meta http-equiv="refresh" content="5" >
	</head>
	<body>
	Your work is being processed. Please be patient
	</body>
	</html>
	'''
	web.write(text)
	web.close()
	return 1

	
def codost(input,databasetext):
	if os.path.isfile('../htdocs/codost_out/sign.txt'):
		os.system('rm ../htdocs/codost_out/sign.txt')
	code1a='python /home/ogun/scripts/CoDoST.py -i '+input+' -d '+databasetext
	code1b=' && cp ../htdocs/codost_out/sig.txt ../htdocs/codost_out/sign.txt'
	code1='nohup '+code1a+code1b

	os.popen(code1)
	signpresent=os.path.isfile('../htdocs/codost_out/sign.txt')
	while not signpresent:
		print '<p>Z</p>'
		time.sleep(5)
	
def table2domfigure(outhtml):
	code2='python /home/ogun/scripts/table2domfigure.py -i ../htdocs/codost_in/fastainput.fa -o ../htdocs/codost_out/'+outhtml
	code3=" && mv *.table ../htdocs/codost_out/"
	code4=code2+code3
	os.popen(code4)
	

	



print "Content-type:text/html \r\n\r\n"
print "<html>"
print "<head>"
print "<title>CoDoST"
print "</title>"
print '''<script>
function newDoc()
{
alert("We are about to start");
window.location.assign("'''+filename+'''");
}

</script>'''
link="http://leonidas.bio.utk.edu/codost_out/"+outhtml

print "</head>"
print '<body onload=newDoc()>'
print "<h2>CoDoST</h2>"
print "<p>We are working on your request...</p>"
print "<p>After we are done, you will be directed to results page.</p>"
print "<p></p>"
print "<p>Be patient... It takes 2-3 minutes for each sequence to scan 3 databases</p>"
print "<p>Click <a href="+link+" target=\"_blank\">HERE</a> for results!</p>"
print "<p>You can either wait or save this link and go to your results after some time:</p>"
print "<p><a href="+link+" target=\"_blank\">"+link+"</a></p>"



if not textbox:
	intheprocesspage(outhtml)
	d,l=fastareader("../htdocs/codost_in/fastainput.fa")
	for header in l:
		tem=open("../htdocs/codost_in/temporary.fa",'w')
		tem.write('>')
		tem.write(header)
		tem.write("\n")
		tem.write(d[header])
		tem.close()
		print "<p>"+header+"</p>"
		codost("../htdocs/codost_in/temporary.fa",text)
	
	table2domfigure(outhtml)	
elif sequence and textbox:	
	intheprocesspage(outhtml)
	fas=open("../htdocs/codost_in/fastainput.fa",'w')
	fas.write(sequence)
	fas.close()
	
	d,l=fastareader("../htdocs/codost_in/fastainput.fa")
	for header in l:
		tem=open("../htdocs/codost_in/temporary.fa",'w')
		tem.write('>')
		tem.write(header)
		tem.write("\n")
		tem.write(d[header])
		tem.close()
		print "<p>"+header+"</p>"
		codost("../htdocs/codost_in/temporary.fa",text)
	
	table2domfigure(outhtml)
else:
	thereisnoinput()

	
	
print "</body>"
print "</html>"
	
