#!/usr/bin/python3


# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time
from glob import glob
import re

sys.path.append('/home/ogun/.local/lib/python3.2/site-packages')

print ("Content-type: text/html\n\n") 
print ("")
print ("<html>")
print ("<meta http-equiv='refresh' content='0; url=../MapBurst/highlight.html?f=temp.csv.noded.json' />")
arguments = cgi.FieldStorage()
#for i in arguments.keys():
#	print arguments[i].value

#d = arguments['d'].value
q = arguments['q'].value
#print(str(q) + "\n")
#code = "python3 SUN_csv2json.py -d " + str(d)

code = "python3 /home/ogun/scripts/SUN_query.py -if " + str(q) + " 2>error 1>log 0>input"


os.system(code)


#os.system("echo $PYTHONPATH")
#print (code + "\n")
print ("done")
#print(sys.path)
print ("</html>")
