#!/usr/bin/python3.4
import cgi, cgitb
from urllib.request import urlopen
#from urllib2 import urlopen
import SeqDepot
#import SeqDepot_p2 as SeqDepot

print ("Content-type:text/html\r\n\r\n")
#print ("Content-type:image/png\r\n\r\n")

d = {}
arguments = cgi.FieldStorage()
for i in arguments.keys():
	d[i] = arguments[i].value


ncbi = 0
embl = 0

if 'ncbi_id' in d.keys():
	ncbi = 1
	ncbi_id = d['ncbi_id']
	fastaurl = "http://www.ncbi.nlm.nih.gov/protein/" + ncbi_id  + "?report=fasta"
elif 'uni_id' in d.keys():
	embl = 1
	id = d['uni_id']
	#out = open('deneme.txt','w')
	fastaurl = "http://www.uniprot.org/uniprot/" + id + ".fasta";
elif 'l' in d.keys():
	embl = 1
	fastaurl = d['l']

sequence = ''

if ncbi == 1:
	from Bio import Entrez
	Entrez.email ="ogunadebali@gmail.com"
	handle = Entrez.efetch(db="protein", id=ncbi_id, retmode="xml")
	records = Entrez.read(handle)
	sequence = records[0]["GBSeq_sequence"].upper()

elif embl == 1:
	rl = urlopen(fastaurl).read().decode("utf-8").split('\n')
	for i in range(1,len(rl)):
		sequence += rl[i]

aseqid = SeqDepot.aseqIdFromSequence(sequence)
remoteurl = 'http://seqdepot.net/api/v1/aseqs/' + aseqid + '.png'

#pngtext = urlopen(remoteurl).read()

imgtext = '<img src="' + remoteurl + '" onError="this.onerror=null;this.src=\'../noimage.png\';"></img>'
#print(sequence)
print(imgtext)
#print(pngtext)
#print("Status: 303 See other")
#print('<meta http-equiv="refresh" content="0; url="' + remoteurl + '" />')

#out.write(sequence)
#out.close()
