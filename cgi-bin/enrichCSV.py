#!/usr/bin/python
from pymongo import MongoClient
import cgi, cgitb
import json
print("Content-type:text/html\r\n\r\n")

d = {}
arguments = cgi.FieldStorage()
for i in arguments.keys():
	d[i] = arguments[i].value


db = 'ENS_E80'
file = d['f']

#file = "blast_results/file0.csv"

nameCol = MongoClient()["taxonomy"]["name"]
idsCol = MongoClient()[db]["ids"]


filein = open("ALLBLASTOUT_E80/" + file,'r')
header = filein.readline()

hll = header.split(',')
hll.append('gene')
hll.append('taxid')
hll.append('sn')

newheader = header.strip() + ',' + 'gene' + ',' + 'taxid' + ',' + 'sn' + '\n'

newlines = []
for line in filein:
	ll = line.split(',')
	sseqid = ll[1]
	record = idsCol.find_one({"_id":sseqid})
	#print(record)
	ll.append(record['gene'])
	taxid = record['taxid']
	ll.append(taxid)
	ll.append( nameCol.find_one({"_id":taxid})['sn'])
	#newline = line.strip() + ',' + gene + ',' + str(taxid) + ',' + sn
	newlines.append({hll[0].strip():ll[0].strip(),hll[1].strip():ll[1].strip(),hll[2].strip():ll[2].strip(),hll[3].strip():ll[3].strip(),hll[4].strip():ll[4].strip(),hll[5].strip():ll[5].strip(),hll[6].strip():ll[6].strip(),hll[7].strip():ll[7].strip(),hll[8].strip():ll[8].strip(),hll[9].strip():ll[9].strip(),hll[10].strip():ll[10].strip(),hll[11].strip():ll[11].strip(),hll[12].strip():ll[12],hll[13].strip():ll[13],hll[14].strip():ll[14]})
#	print(newline)
j = json.dumps(newlines,indent=1)
print (j)


