#!/usr/bin/python
from pymongo import MongoClient
import cgi, cgitb
import json
print "Content-type:text/html\r\n\r\n"

d = {}
arguments = cgi.FieldStorage()
for i in arguments.keys():
	d[i] = arguments[i].value


database = d['d']
col = d['c']
if 'nid' in arguments.keys():
	id = int(d['nid'])
else:
	id = d['id']

#a = MongoClient()[database]

j = MongoClient()[database][col].find_one({"_id":id})

print json.dumps(j,indent=1)


