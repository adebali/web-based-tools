#!/usr/bin/env python


import os
import sys
import datetime
import re
import codostlib as co
import subprocess
from glob import glob

timestart=datetime.datetime.now()

arg=[]
arg = sys.argv
help = "Help Info\n__________\nThis script takes fasta file as an input, reads headers, finds the tables, and uses the tables as inputs"
help = help+"\n\nusage: table2domfigure.py -i yourfastafile.fa \n\noptional: \n-o output.html"
help = help+"\n\nPS: In your fasta file first 50 characters of each header must be unique!\n"


def getJobID(ID):
	filein = open("STATUS"+ID,"r")
	filein.readline()
	filein.readline()
	jobID = int(filein.readline()[0:8])
	return str(jobID)

def checkPossibleError(JobID,proteinNumber):
	qstatinfo = subprocess.check_output(['ssh'],['newton'],['\'qstat\''])
	a= 'CODOST_20140517173857289529.e762207.33'


if '-h' in sys.argv:
        print help+"\n"
        sys.exit()

if '-i' in sys.argv:
        inputfile=sys.argv[sys.argv.index('-i')+1]
        seq_dic, seq_list = co.fastareader(inputfile)
else:
        print "No input\n"
        sys.exit()

if '--empty' in sys.argv:
	empty = 1
	jobId = ''
else:
	empty = 0

if '--kill' in sys.argv:
	kill = 1
else:
	kill = 0

if '-o' in sys.argv:
	outputfile =sys.argv[sys.argv.index('-o')+1]
else:
	outputfile = inputfile[:(inputfile.rindex('.')-len(inputfile))]+'.html'


if '--print' in sys.argv:
	printsign=1
else:
	printsign=0		

if '-newton' in sys.argv:
	newton=1
	ID=sys.argv[sys.argv.index('-newton')+1]
	if empty:
		requestNotTaken = [1]
	else:
		requestNotTaken = glob("/var/www/localhost/htdocs/requests/FASTA" + ID + ".fa")
	
		if requestNotTaken==[]:
			requestTaken = 1
			os.system("bash /var/www/localhost/cgi-bin/getSTATUS.sh " +ID)
			STATUSFILE = "STATUS" + ID
			jobID= getJobID(ID)
		else:
			requestTaken = 0
else:
	newton=0
	
if '-param' in sys.argv:
	param=1
	parameterfile=sys.argv[sys.argv.index('-param')+1]
else:
	param=0
	
def cprint(thetexttobeprinted):
	if printsign:
		print thetexttobeprinted
		return
	else:
		return

if kill==1:
	#jobID = getJobID(ID)
	code = "ssh newton qdel " + jobID + " >qdel.log"
	os.system(code)

letter2status ={}
letter2status['queued'] = "queued"
letter2status['q'] = "queued"
letter2status['qw'] = "queued"
letter2status['r'] = "running"

def getStatusDictionary():
	task_status_Dict={}
	statusfile = open(STATUSFILE,"r")
	statusfile.readline()
	statusfile.readline()
	for line in statusfile:
		ll = line.split(' ')
	#	print ll[-1]
		taskID = int(ll[-1])
		llll = []
		lll = line.split(' ')
		for ele in lll:
			ele=ele.strip()
			if ele:
				llll.append(ele)
		task_status_Dict[str(llll[9])] = llll[4]

	return task_status_Dict
	
def getStatus(proteinnumber,task_status_Dict):
	try:
		status = task_status_Dict[str(proteinnumber)]
	except:
		status = "queued"

	return letter2status[status]

	


def table2features(table_in):
	table=open(table_in,'r')
	lines=table.readlines()
	
	header=lines[0].replace('\n','')
	fullsequence=lines[1].replace('\n','')
	TMinfo=lines[2].replace('\n','')
	del lines[0]
	del lines[0]
	del lines[0]
	tag=co.removesymbols(header)[:50]
	
	seqlength=len(fullsequence)
	
	features='\''+tag+'\''+','+'\''+str(fullsequence)+'\''+','+'\''+str(TMinfo)+'\''+','
	domains=''
	intervals=''
	scores=''
	databases=''
	coverages=''
	sequences=''
	alignments=''
	
	
	for line in lines:
		if line:
			if not 'XXXX' in line.split('\t')[0]: 
				
				domains=domains+line.split('\t')[0]+';'
				intervals=intervals+line.split('\t')[1]+';'
				scores=scores+line.split('\t')[2]+';'
				databases=databases+line.split('\t')[3]+';'
				sequences=sequences+line.split('\t')[5].replace("\n","")+';'
				alignments=alignments+line.split('\t')[6].replace("\n","").replace(" ","_")+';'
				
				
				cov=line.split('\t')[4].split('|')[0].strip()+'_'+line.split('\t')[4].split('|')[1].strip()+'_'+line.split('\t')[4].split('|')[2].strip()
				
				cprint (cov+'\n')
				coverages=coverages+cov+';'
			
		
	features=features+'\''+domains+'\''+','+'\''+intervals+'\''+','+'\''+scores+'\''+','+'\''+databases+'\''+','+'\''+coverages+'\''+','+'\''+sequences+'\''+','+'\''+alignments+'\''
	cprint (coverages)
	#print features
	return features	




def htmlprint_initialize():
	return '''
<html>
	<head>
		<LINK REL="SHORTCUT ICON"  HREF="http://web.utk.edu/~oadebali/CoDAT.ico">
		<title>Domain Architecures</title>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
		<style>
		#header
			{
			top: 0;
			display:block;
			background: white;
			}
		#header.fixed
			{
			position:fixed; 
			top: 0;  /*fixing it at the top*/
			z-index: 999;  /* over any other element*/
			}	
		.firstrow
			{
			width:115;
			}
		.secondrow
			{
			width:115;
			vertical-align: top;
			}

		</style>
		<SCRIPT LANGUAGE="JavaScript">
		</SCRIPT>
			<script src="http://web.utk.edu/~oadebali/architecturesvg.js"></script>
			<script src="http://web.utk.edu/~oadebali/jquery-ui.js"></script>'''

def htmlprint_scriptForInputForm(refreshTime):
	return '<script> setTimeout("document.search_form.submit()",' + str(refreshTime) + '000);</script>'
	
def htmlprint_closeHeadOpenBody():
	return '''
	</head>
	 
	 <body link="black">
	
'''

def htmlprint_majorButtonsTable():
	return '''
			<table id="header" border="0">
					<tr style="height:15px;">
				<td class="firstrow"><img src="http://web.utk.edu/~oadebali/codost.png" width="100px" height="auto"></td>

<!-- 				<td id='tablecolorcode' rowspan="2">
				<svg id='colorcode' width="360px" height="80px"><script>drawcolorcode()</script></svg>
				</td> -->

				
				<td class="Stm firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".tm").show();$(".Stm").hide();$(".Htm").show()'><font size="1"/><b/>Show TM</button></td>
				<td class="Htm firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".tm").hide();$(".Stm").show();$(".Htm").hide()'><font size="1"/><b/>Hide TM</button></td>
				
				<td class="Scov firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".coverage").show();$(".Scov").hide();$(".Hcov").show()'><font size="1"/><b/>Show Coverage</button></td>
				<td class="Hcov firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".coverage").hide();$(".Scov").show();$(".Hcov").hide()'><font size="1"/><b/>Hide Coverage</button></td>
				
				<td class="Sali firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".alignment").show();$(".Sali").hide();$(".Hali").show()'><font size="1"/><b/>Show Alignment</button></td>
				<td class="Hali firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".alignment").hide();$(".Sali").show();$(".Hali").hide()'><font size="1"/><b/>Hide Alignment</button></td>
				
				<td class="Sseq firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".sequence").show();$(".Sseq").hide();$(".Hseq").show()'><font size="1"/><b/>Show Sequence</button></td>
				<td class="Hseq firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".sequence").hide();$(".Sseq").show();$(".Hseq").hide()'><font size="1"/><b/>Hide Sequence</button></td>
				
				<td class="Sarc firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".mainarch").show();$(".Sarc").hide();$(".Harc").show()'><font size="1"/><b/>Show Domains</button></td>
				<td class="Harc firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".mainarch").hide();$(".Sarc").show();$(".Harc").hide()'><font size="1"/><b/>Hide Domains</button></td>
				
				<!--<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='myreset();'><font size="1"/><b/>Reset</button></td>-->
				
				<td class="Sdra firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='drag();$(".Sdra").hide();$(".Sfix").show()'><font size="1"/><b/>Drag</button></td>
				<td class="Sfix firstrow"><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='dragfix();$(".Sdra").show();$(".Sfix").hide()'><font size="1"/><b/>Fix</button></td>
				<td width="75" align="center" valign="center"><a href="#" onClick=" window.print(); return false" STYLE="TEXT-DECORATION: NONE"><font color="black" size="2"><b/>PDF</font></a></td>
			
		
			</tr>
		'''
def htmlprint_refreshAndStatusTable():
	text =  '''
			<tr style="height:15px;">
			<td></td>
			<td class="secondrow">
				<form name="search_form" method="post" action="/cgi-bin/cgi2.py?htmlID='''
	text += ID
	text += '''" enctype="multipart/form-data">
        	                <input  id="refreshButton" name="Submit" type="submit" value="Status Check" style="width:115px; height: 20px; valign:top, font-size:8px"/>

			</td>
			<td width="500" valign="top" id="statusbar" class="statusbars" colspan="4"></td>
			<td class="secondrow">
				<input  id="killButton" name="Submit" type="button" value="Kill Jobs" style="width:115px; height: 20px; valign:top, font-size:8px"/>
				</form>
			</tr>'''
	return text
			
def htmlprint_closeHeaderTableOpenMainTable():
	return'''
		</table>
		<script>
			//$(document).ready(function(){ $("[id*='refreshButton']").click();});

			//by default, the static menu is hidden
			var showStaticMenuBar = false;
			//when scrolling...
			$(window).scroll(function () {
			//if the static menu is not yet visible...
			if (showStaticMenuBar == false) {
				//if I scroll more than 200px, I show it 
				if ($(window).scrollTop() >= 10) {
					//showing the static menu
					$('#header').addClass('fixed');
					showStaticMenuBar = true
				}
			}
			//if the static menu is already visible...
			else {
				if ($(window).scrollTop() <10) {
					$('#header').removeClass('fixed');
					//I define it as hidden
					showStaticMenuBar = false;
				}
			}
			});
		</script>
		<table border="0", style='table-layout:fixed'>
	'''

def htmlprint_statusRow(tablename,proteincount,status):
	
	text = '<tr><td><td>'
        text=text+'<td valign="center"> ' + str(tablename) + '</td><td class="statusbars"><script>drawSinlgeStatus("'+str(status)+'");</script></td>'
	return text

def htmlprint_domainArchitectureRow(canvaslength,features,tablename,proteincount):
	list=features.split(",")
	tag=list[0]
	del list[0]
	
	text= '''<tr>'''
	
	if newton:
		text=text+'<td><a href=CODOST_'+ID+'.o'+jobID+'.'+str(proteincount)+'>o</a></td>'
		text=text+'<td><a href=CODOST_'+ID+'.e'+jobID+'.'+str(proteincount)+'>e</a></td>'
	else:
		text=text+'<td></td><td></td>'
				
				
	text=text+'''
<td><a href="JavaScript:newpopup('
'''
	text=text+tablename
	text=text+'''',536,954);">'''
	text=text+tag		
	text=text+'''</a></td>
	
	<td width="'''+str(canvaslength)+'''">
	
	<svg class='backbone draggable ' id='''
	text=text+tag	
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawbackbone('''
	text=text+features
	text=text+''')</script>

	<svg class='tm' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">			
	<script>drawTM('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	<svg class='mainarch' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">			
	<script>drawarch('''
	text=text+features
	text=text+''')</script>
	</svg>
				
	<svg class='alignment' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawalignment('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	<svg class='sequence' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawsequence('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	<svg class='coverage' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawcoverage('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	</svg>					
	</td>			
	</tr>
	'''
	return text

def htmlprint_JSforStatusGraph(status_dict):
	q = status_dict['queued']
	r = status_dict['running']
	c = status_dict['completed']
	k = status_dict['killed']
	text = '''<script type="text/javascript">
	var bar = document.getElementById('statusbar').innerHTML;'''

	text += 'drawStatusBar('+str(q)+','+str(r)+','+str(c)+','+str(k)+');</script>'
	
	return text
	
def htmlprint_closeMainTable():
	return '''
		<script>myreset();</script>
		
		</table>
		'''

def htmlprint_parameterLine():
	pfile=open(parameterfile,'r')
	text='<p style="color:grey">'+pfile.readlines()[0]+'</p>'
	return text
	
def htmlprint_end():
	return '''
	</body>
</html>
		'''
	



TableCount = len(glob('/var/www/localhost/htdocs/codost_out/' + ID + "*.table"))
TotalProteinCount = len(seq_list)
complete = 0
if TableCount == TotalProteinCount:
	with open('note.txt','w') as f:
		f.write(str(TableCount))
	complete = 1
else:
	complete = 0

if empty:
	statuspage = 1
else:
	if complete:
		statuspage = 0
	else:
		statuspage = 1

cprint(outputfile)
out=open(outputfile,'w')		
out.write(htmlprint_initialize())

if statuspage==1 and empty==0 and requestTaken==1:
	task_stat_Dict = getStatusDictionary()

#complete = 0
if complete != 1:
	if empty ==1:
		refreshTime = 20
	else:
		refreshTime = 100
	out.write(htmlprint_scriptForInputForm(refreshTime))

out.write(htmlprint_closeHeadOpenBody())
out.write(htmlprint_majorButtonsTable())

if complete != 1 and kill == 0:
	out.write(htmlprint_refreshAndStatusTable())

out.write(htmlprint_closeHeaderTableOpenMainTable())
	
	
proteincount=1

status_dict={}
status_dict['queued'] = status_dict['running'] = status_dict['completed'] = status_dict['killed'] = 0 



for proteinheader in seq_list:
	header=co.removesymbols(proteinheader)[0:50]
	filename=str(ID)+header+'.table'
	fullpathOfTable = '/var/www/localhost/htdocs/codost_out/' + filename
	#print filename
	tableExist = glob(fullpathOfTable)
	if empty:
		status = "queued"
		out.write(htmlprint_statusRow(header,proteincount,status))
	else:
		if tableExist != []:
			if newton:
				features=table2features(fullpathOfTable)
				canvaslength=len(seq_dic[proteinheader])+5
				out.write(htmlprint_domainArchitectureRow(canvaslength,features,filename,proteincount))
				status = "completed"

		elif kill:
			status = "killed"
			out.write(htmlprint_statusRow(header,proteincount,status))
		elif requestTaken:
			status = getStatus(proteincount,task_stat_Dict)
			out.write(htmlprint_statusRow(header,proteincount,status))
		else:
			status = "queued"
			out.write(htmlprint_statusRow(header,proteincount,status))
	status_dict[status] += 1

	proteincount=proteincount+1

out.write(htmlprint_closeMainTable())

if param:
	out.write(htmlprint_parameterLine())
out.write(htmlprint_JSforStatusGraph(status_dict))
out.write(htmlprint_end())
out.close()

timeend=datetime.datetime.now()

timepassed=timeend-timestart

cprint("Time spent:")
cprint(str(timepassed))



